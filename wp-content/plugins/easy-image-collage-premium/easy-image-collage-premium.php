<?php
/*
Plugin Name: Easy Image Collage Premium
Plugin URI:
Description: Create beautiful responsive image collages for all your posts and pages
Version: 1.10.0
Author: Bootstrapped Ventures
Author URI: http://bootstrapped.ventures
License: GPLv2
*/

define( 'EIC_PREMIUM_VERSION', '1.10.0' );

class EasyImageCollagePremium {

	private static $instance;

	/**
	 * Return instance of self
	 */
	public static function get()
	{
		// Instantiate self only once
		if( is_null( self::$instance ) ) {
			self::$instance = new self;
			self::$instance->init();
		}

		return self::$instance;
	}

	public $premiumName = 'easy-image-collage-premium';
	public $premiumDir;
	public $premiumPath;
	public $premiumUrl;

	private $eic;

	/**
	 * Our only task is to correctly set up WP Ultimate Recipe and load the premium helpers and addons
	 */
	public function init()
	{
		$this->premiumPath = str_replace( '/easy-image-collage-premium.php', '', plugin_basename( __FILE__ ) );
		$this->premiumDir = apply_filters( 'eic_premium_dir', WP_PLUGIN_DIR . '/' . $this->premiumPath . '/premium' );
		$this->premiumUrl = apply_filters( 'eic_premium_url', plugins_url() . '/' . $this->premiumPath . '/premium' );

		add_filter( 'eic_core_dir', array( $this, 'filter_eic_core_dir' ) );
		add_filter( 'eic_core_url', array( $this, 'filter_eic_core_url' ) );
		add_filter( 'eic_plugin_file', array( $this, 'filter_eic_plugin_file' ) );

		// Include and instantiate WP Ultimate Post Grid
		require_once( WP_PLUGIN_DIR . '/' . $this->premiumPath . '/core/easy-image-collage.php' );
		$this->eic = EasyImageCollage::get( true );

		// Load textdomain
		$domain = 'easy-image-collage';
		$locale = apply_filters('plugin_locale', get_locale(), $domain);

		load_textdomain($domain, WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . $locale . '.mo');
		load_plugin_textdomain($domain, false, $this->premiumPath . '/core/lang/');

		// Add Premium helper directory
		$this->eic->add_helper_directory($this->premiumDir . '/helpers');

		// Load Premium helpers
		$this->eic->helper('license');
		$this->eic->helper('premium_layouts');

		// Load Premium addons
		$this->eic->helper('addon_loader')->load_addons($this->premiumDir . '/addons');

		// Add plugin action links
        add_filter('plugin_action_links_' . $this->premiumPath . '/easy-image-collage-premium.php', array($this->eic->helper('plugin_action_link'), 'action_links'));
	}

	public function filter_eic_core_dir()
	{
		return WP_PLUGIN_DIR . '/' . $this->premiumPath . '/core';
	}

	public function filter_eic_core_url()
	{
		return plugins_url() . '/' . $this->premiumPath . '/core';
	}

	public function filter_eic_plugin_file()
	{
		return __FILE__;
	}
}

// Check if Easy Image Collage isn't activated
if( class_exists( 'EasyImageCollage' ) ) {
	wp_die( __( "You need to deactivate the free Easy Image Collage plugin before activating the Premium version. You won't lose any settings or grids when deactivating.", 'easy-image-collage' ), 'Easy Image Collage Premium', array( 'back_link' => true ) );
} else {
	// Instantiate Easy Image Collage Premium
	EasyImageCollagePremium::get();
}