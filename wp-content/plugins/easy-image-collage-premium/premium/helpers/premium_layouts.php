<?php

class EIC_Premium_Layouts {

    public function __construct()
    {
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin' ) );
    }

    public function enqueue_admin()
    {
        $screen = get_current_screen();

        if( $screen->base == 'post' ) {
            wp_enqueue_script( 'eic_admin_premium', EasyImageCollagePremium::get()->premiumUrl . '/js/admin.js', array( 'eic_admin' ), EIC_VERSION, true );
        }
    }
}