var EasyImageCollage = EasyImageCollage || {};

EasyImageCollage.redrawDividers = function() {
    if(EasyImageCollage.editing_grid['dividers'] != undefined) {
        for(var i = 0; i < EasyImageCollage.editing_grid['dividers'].length; i++) {
            var pos_perc = EasyImageCollage.editing_grid['dividers'][i];

            if(pos_perc !== undefined && pos_perc) {
                pos_perc = pos_perc * 100;
                var divider = jQuery('.eic-lightbox .eic-editing .eic-divider-' + i);

                var type = divider.data('divider-type');
                if(type == 'row') {
                    divider.css('top', pos_perc + '%');
                    divider.siblings('.eic-child-1')
                        .css('bottom', pos_perc + '%')
                        .css('height', pos_perc + '%');
                    divider.siblings('.eic-child-2')
                        .css('top', pos_perc + '%')
                        .css('height', (100 - pos_perc) + '%');
                }  else {
                    divider.css('left', pos_perc + '%');
                    divider.siblings('.eic-child-1')
                        .css('right', pos_perc + '%')
                        .css('width', pos_perc + '%');
                    divider.siblings('.eic-child-2')
                        .css('left', pos_perc + '%')
                        .css('width', (100 - pos_perc) + '%');
                }
            }
        }
    }

    jQuery('.eic-lightbox .eic-editing .eic-divider').each(function() {
        EasyImageCollage.handleDividerMove(jQuery(this), EasyImageCollage.editing_grid);
    });
};

EasyImageCollage.handleDividerMove = function(divider, grid) {
    divider.on('mousedown touchstart', function(e) {
        if (e.target !== divider[0]) return;
        e.preventDefault();

        if (e.originalEvent.touches) {
            EasyImageCollage.modifyEventForTouch(e);
        } else if (e.which !== 1) {
            return;
        }
        
        var type = divider.data('divider-type'),
            id = divider.data('divider-id');

        if(type == 'row') {
            var start = e.clientY,
                pos = parseFloat(divider.css('top')),
                parent = divider.parent().innerHeight();

        }  else {
            var start = e.clientX,
                pos = parseFloat(divider.css('left')),
                parent = divider.parent().innerWidth();
        }

        var pos_perc = (pos / parent) * 100,
            margin = 50,
            top_right_margin = margin + margin * divider.siblings('.eic-child-2').find('.eic-divider-' + type).length;

        jQuery(window).on('mousemove touchmove', function(e) {
            e.preventDefault();

            if (e.originalEvent.touches) {
                EasyImageCollage.modifyEventForTouch(e);
            }

            if(type == 'row') {
                var current = e.clientY;
            }  else {
                var current = e.clientX;
            }

            // New position
            pos = pos+current-start;

            // Check bounds
            pos = pos < margin ? margin : ( pos > parent - top_right_margin ? parent - top_right_margin : pos );
            pos_perc = (pos / parent) * 100;

            // New starting point for drag
            start = current;

            if(type == 'row') {
                divider.css('top', pos_perc + '%');
                divider.siblings('.eic-child-1')
                    .css('bottom', pos_perc + '%')
                    .css('height', pos_perc + '%');
                divider.siblings('.eic-child-2')
                    .css('top', pos_perc + '%')
                    .css('height', (100 - pos_perc) + '%');
            }  else {
                divider.css('left', pos_perc + '%');
                divider.siblings('.eic-child-1')
                    .css('right', pos_perc + '%')
                    .css('width', pos_perc + '%');
                divider.siblings('.eic-child-2')
                    .css('left', pos_perc + '%')
                    .css('width', (100 - pos_perc) + '%');
            }

            EasyImageCollage.redrawImages();
        });

        jQuery(window).on('mouseup touchend', function() {
            // Update new divider position
            var type = divider.data('divider-type'),
                id = divider.data('divider-id');

            if(type == 'row') {
                var pos = parseFloat(divider.css('top')),
                    parent = divider.parent().innerHeight();

            }  else {
                var pos = parseFloat(divider.css('left')),
                    parent = divider.parent().innerWidth();
            }

            if(grid['dividers'] == undefined) {
                grid['dividers'] = [];
            }
            grid['dividers'][id] = pos / parent;

            // Remove event handlers
            jQuery(window).off('mousemove touchmove');
            jQuery(window).off('mouseup touchend');
        });
    });
};

EasyImageCollage.recalculateSizes = function() {
    jQuery('.eic-image').each(function() {
        var image = jQuery(this),
            width = image.innerWidth(),
            height = image.innerHeight();
        
        image.find('.eic-image-width').text(width);
        image.find('.eic-image-height').text(height);
    });
};