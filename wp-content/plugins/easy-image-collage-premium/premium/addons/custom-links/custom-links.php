<?php

class EIC_Custom_Links extends EIC_Premium_Addon {

    public function __construct( $name = 'custom-links' ) {
        parent::__construct( $name );

        // Actions
	    add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin' ) );
    }

	public function enqueue_admin()
	{
		$screen = get_current_screen();

		if( $screen->base == 'post' ) {
			// Plugin assets
			wp_enqueue_script( 'eic_custom_links', $this->addonUrl . '/js/custom-links.js', array( 'eic_admin' ), EIC_VERSION, true );
		}
	}
}

EasyImageCollage::loaded_addon( 'custom-links', new EIC_Custom_Links() );