var EasyImageCollage = EasyImageCollage || {};

EasyImageCollage.loadCustomLinks = function(id) {
    var grid = EasyImageCollage.editing_grid;

    var table = jQuery('.eic-lightbox .eic-links-images');
    var table_contents = '';

    for(var i = 0; i < grid['images'].length; i++) {
        var image = grid['images'][i];

        if(image) {
            var src = image.attachment_thumb == undefined ? image.attachment_url : image.attachment_thumb;
            var link = image.custom_link == undefined ? '' : image.custom_link;
            console.log(image);
            var link_new_tab = image.custom_link_new_tab == undefined ? eic_admin.default_link_new_tab : image.custom_link_new_tab;
            var link_nofollow = image.custom_link_nofollow == undefined ? eic_admin.default_link_nofollow : image.custom_link_nofollow;

            link_new_tab = link_new_tab == 'false' ? false : link_new_tab;
            link_nofollow = link_nofollow == 'false' ? false : link_nofollow;

            table_contents += '<tr data-image="' + i + '">';
            table_contents += '<td><img src="' + src + '"></td>';
            table_contents += '<td>';
            table_contents += '<input type="text" id="eic-links-images-link-' + i + '" class="eic-links-images-link" value="' + link + '">';
            table_contents += '<input type="checkbox" id="eic-links-images-link-new-tab-' + i + '" class="eic-links-images-link-new-tab"';
            if(link_new_tab) table_contents += ' checked';
            table_contents += '><label for="eic-links-images-link-new-tab-' + i + '">' + eic_admin.text_link_new_tab + '</label>';
            table_contents += '<input type="checkbox" id="eic-links-images-link-nofollow-' + i + '" class="eic-links-images-link-nofollow"';
            if(link_nofollow) table_contents += ' checked';
            table_contents += '><label for="eic-links-images-link-nofollow-' + i + '">' + eic_admin.text_link_nofollow + '</label>';
            table_contents += '</td>';
            table_contents += '</tr>';
        }
    }

    table.html(table_contents);
    jQuery('#eic-links-images-link-' + id).focus();
};

EasyImageCollage.btnCustomLinksSave = function() {
    var grid = EasyImageCollage.editing_grid;

    for(var i = 0; i < grid['images'].length; i++) {
        var image = grid['images'][i];

        if(image) {
            image.custom_link = jQuery('#eic-links-images-link-' + i).val();
            image.custom_link_new_tab = jQuery('#eic-links-images-link-new-tab-' + i).prop('checked');
            image.custom_link_nofollow = jQuery('#eic-links-images-link-nofollow-' + i).prop('checked');

            EasyImageCollage.editing_grid['images'][i] = image;
        }
    }

    EasyImageCollage.setActivePage('editing');
};