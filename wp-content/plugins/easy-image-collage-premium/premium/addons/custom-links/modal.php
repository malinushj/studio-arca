<a href="#" class="eic-modal-title" onclick="event.preventDefault(); EasyImageCollage.setActivePage('editing')"><i class="fa fa-angle-double-left"></i> <?php _e( 'Cancel', 'easy-image-collage' ); ?></a>
<a href="#" class="eic-modal-title eic-modal-title-right" onclick="event.preventDefault(); EasyImageCollage.btnCustomLinksSave()"><?php _e( 'Save Changes', 'easy-image-collage' ); ?> <i class="fa fa-angle-double-right"></i></a>

<table class="eic-links-images">
</table>