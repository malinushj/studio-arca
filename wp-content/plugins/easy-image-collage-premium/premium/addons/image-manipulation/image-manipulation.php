<?php

class EIC_Image_Manipulation extends EIC_Premium_Addon {

    public function __construct( $name = 'image-manipulation' ) {
        parent::__construct( $name );

        // Actions
	    add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin' ) );

        // AJAX
        add_action( 'wp_ajax_manipulate_image', array( $this, 'ajax_manipulate_image' ) );
    }

	public function enqueue_admin()
	{
		$screen = get_current_screen();

		if( $screen->base == 'post' ) {
			// Vendor assets
			wp_enqueue_script( 'camanjs', EasyImageCollagePremium::get()->premiumUrl . '/vendor/CamanJS/caman.full.js', EIC_VERSION, true );

			// Plugin assets
			wp_enqueue_script( 'eic_image_manipulation', $this->addonUrl . '/js/image-manipulation.js', array( 'eic_admin', 'camanjs' ), EIC_VERSION, true );
		}
	}

    public function ajax_manipulate_image()
    {
        if( check_ajax_referer( 'eic_image_collage', 'security', false ) )
        {
            $image_data =  explode( ',', $_POST['image'], 2 );
            preg_match("/data:image\/([^;]+)/i", $image_data[0], $filetype);

            $attachment_id = $this->upload_base64_image($image_data[1], $filetype[1]);
            $full = wp_get_attachment_image_src( $attachment_id, 'full' );
            $thumb = wp_get_attachment_image_src( $attachment_id, 'medium' );

            $image = array(
                'attachment_id' => $attachment_id,
                'attachment_url' => $full[0],
                'attachment_width' => $full[1],
                'attachment_height' => $full[2],
                'attachment_thumb' => $thumb[0],
            );

            echo json_encode($image);
        }

        die();
    }

    public function upload_base64_image( $item, $filetype ) {
        $upload_dir = wp_upload_dir();
        $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

        $decoded = base64_decode( (string) $item );
        $filename = time() . '.' . strtolower( $filetype );
        $image_upload = file_put_contents( $upload_path . $filename, $decoded );

        if( !function_exists( 'wp_handle_sideload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
        if( !function_exists( 'wp_get_current_user' ) ) require_once( ABSPATH . 'wp-includes/pluggable.php' );

        $file             = array();
        $file['error']    = '';
        $file['tmp_name'] = $upload_path . $filename;
        $file['name']     = $filename;
        $file['type']     = 'image/' . strtolower( $filetype );
        $file['size']     = filesize( $upload_path . $filename );

        $file_return      = wp_handle_sideload( $file, array( 'test_form' => false ) );

        $filename = $file_return['file'];
        $attachment = array(
            'post_mime_type' => $file_return['type'],
            'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
            'post_content' => '',
            'post_status' => 'inherit',
            'guid' => $upload_dir['url'] . '/' . basename($filename)
        );

        $attach_id = wp_insert_attachment( $attachment, $filename, 0 );
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
        wp_update_attachment_metadata( $attach_id, $attach_data );

        return $attach_id;
    }
}

EasyImageCollage::loaded_addon( 'image-manipulation', new EIC_Image_Manipulation() );