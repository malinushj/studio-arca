var EasyImageCollage = EasyImageCollage || {};

EasyImageCollage.canvasID = 0;
EasyImageCollage.manipulatingImage = undefined;

EasyImageCollage.loadImageManipulate = function(id) {
    EasyImageCollage.canvasID++;
    var image = EasyImageCollage.editing_grid['images'][id];
    EasyImageCollage.manipulatingImage = image;

    var src = image.attachment_thumb == undefined ? image.attachment_url : image.attachment_thumb;

    var image_elem = jQuery('<img />', {
        id: 'eic-manipulating-image-' + EasyImageCollage.canvasID,
        src: src
    });
    jQuery('.eic-lightbox .eic-manipulating-image').html(image_elem).append(EasyImageCollage.spinner);
    jQuery('.eic-lightbox .eic-manipulating-filters').html('');
    jQuery('.eic-lightbox .eic-manipulating-properties').html('');

    Caman('#eic-manipulating-image-' + EasyImageCollage.canvasID, function() {
        this.render();
    });

    EasyImageCollage.initManipulationFilters();
    EasyImageCollage.initManipulationSliders();
};

EasyImageCollage.btnManipulationSave = function() {
    EasyImageCollage.canvasID++;
    var image_elem = jQuery('<img />', {
        id: 'eic-manipulating-image-' + EasyImageCollage.canvasID,
        src: EasyImageCollage.manipulatingImage.attachment_url
    });
    jQuery('.eic-lightbox .eic-manipulating-image').append(image_elem);
    jQuery('#eic-manipulating-image-' + EasyImageCollage.canvasID).hide();

    EasyImageCollage.redrawManipulatedImageProperties(function(image) {
        var data = {
            action: 'manipulate_image',
            security: eic_admin.nonce,
            image: image.toBase64('jpeg')
        };

        jQuery.post(eic_admin.ajaxurl, data, function(image) {
            for (var attr in image) {
                EasyImageCollage.manipulatingImage[attr] = image[attr];
            }

            EasyImageCollage.editing_grid['images'][EasyImageCollage.manipulatingImage['id']] = EasyImageCollage.manipulatingImage;
            EasyImageCollage.setActivePage('editing');
        }, 'json');
    });
};

EasyImageCollage.redrawManipulatedImageProperties = function(callback) {
    Caman('#eic-manipulating-image-' + EasyImageCollage.canvasID, function() {
        var spinner = jQuery('.eic-lightbox .eic-manipulating-image .eic-spinner');
        var image = this;

        spinner.show();

        image.revert(false);
        image.brightness(parseInt(jQuery('.eic-lightbox .eic-manipulating-properties #brightness').val()));
        image.contrast(parseInt(jQuery('.eic-lightbox .eic-manipulating-properties #contrast').val()));
        image.exposure(parseInt(jQuery('.eic-lightbox .eic-manipulating-properties #exposure').val()));
        image.saturation(parseInt(jQuery('.eic-lightbox .eic-manipulating-properties #saturation').val()));
        image.vibrance(parseInt(jQuery('.eic-lightbox .eic-manipulating-properties #vibrance').val()));
        image.hue(parseInt(jQuery('.eic-lightbox .eic-manipulating-properties #hue').val()));
        image.sepia(parseInt(jQuery('.eic-lightbox .eic-manipulating-properties #sepia').val()));
        image.noise(parseInt(jQuery('.eic-lightbox .eic-manipulating-properties #noise').val()));
        image.sharpen(parseInt(jQuery('.eic-lightbox .eic-manipulating-properties #sharpen').val()));
        image.stackBlur(parseInt(jQuery('.eic-lightbox .eic-manipulating-properties #blur').val()));

        jQuery('.eic-lightbox .eic-manipulating-filter.active').each(function() {
            var filter = jQuery(this).data('filter');
            switch(filter) {
                case 'vintage':     image.vintage(); break;
                case 'sunrise':     image.sunrise(); break;
                case 'lomo':        image.lomo(); break;
                case 'nostalgia':   image.nostalgia(); break;
                case 'clarity':     image.clarity(); break;
                case 'pinhole':     image.pinhole(); break;
                case 'grungy':      image.grungy(); break;
                case 'love':        image.love(); break;
                case 'hemingway':   image.hemingway(); break;
                case 'concentrate': image.concentrate(); break;
            }
        });

        image.render(function() {
            spinner.hide();

            if(typeof callback === 'function') {
                callback(image);
            }
        });
    });
};

EasyImageCollage.initManipulationFilters = function() {
    var filters = jQuery('.eic-lightbox .eic-manipulating-filters-placeholder').html();
    jQuery('.eic-lightbox .eic-manipulating-filters').html(filters);

    jQuery('.eic-lightbox .eic-manipulating-filters .eic-manipulating-filter').on('click', function() {
        jQuery(this).toggleClass('active');
        EasyImageCollage.redrawManipulatedImageProperties();
    });
};

EasyImageCollage.initManipulationSliders = function() {
    var properties = jQuery('.eic-lightbox .eic-manipulating-properties-placeholder').html();
    jQuery('.eic-lightbox .eic-manipulating-properties').html(properties);

    jQuery('.eic-lightbox .eic-manipulating-properties #brightness')
        .simpleSlider({
            range: [-100,100],
            step: 1,
            snap: true
        }).bind('slider:changed', function (event, data) {
            jQuery('.eic-lightbox .eic-manipulating-properties #brightness-value').html(''+data.value);
        }).bind('slider:release', function (event, data) {
            EasyImageCollage.redrawManipulatedImageProperties();
        });
    jQuery('.eic-lightbox .eic-manipulating-properties #contrast')
        .simpleSlider({
            range: [-100,100],
            step: 1,
            snap: true
        }).bind('slider:changed', function (event, data) {
            jQuery('.eic-lightbox .eic-manipulating-properties #contrast-value').html(''+data.value);
        }).bind('slider:release', function (event, data) {
            EasyImageCollage.redrawManipulatedImageProperties();
        });
    jQuery('.eic-lightbox .eic-manipulating-properties #exposure')
        .simpleSlider({
            range: [-100,100],
            step: 1,
            snap: true
        }).bind('slider:changed', function (event, data) {
            jQuery('.eic-lightbox .eic-manipulating-properties #exposure-value').html(''+data.value);
        }).bind('slider:release', function (event, data) {
            EasyImageCollage.redrawManipulatedImageProperties();
        });
    jQuery('.eic-lightbox .eic-manipulating-properties #saturation')
        .simpleSlider({
            range: [-100,100],
            step: 1,
            snap: true
        }).bind('slider:changed', function (event, data) {
            jQuery('.eic-lightbox .eic-manipulating-properties #saturation-value').html(''+data.value);
        }).bind('slider:release', function (event, data) {
            EasyImageCollage.redrawManipulatedImageProperties();
        });
    jQuery('.eic-lightbox .eic-manipulating-properties #vibrance')
        .simpleSlider({
            range: [-100,100],
            step: 1,
            snap: true
        }).bind('slider:changed', function (event, data) {
            jQuery('.eic-lightbox .eic-manipulating-properties #vibrance-value').html(''+data.value);
        }).bind('slider:release', function (event, data) {
            EasyImageCollage.redrawManipulatedImageProperties();
        });
    jQuery('.eic-lightbox .eic-manipulating-properties #hue')
        .simpleSlider({
            range: [0,100],
            step: 1,
            snap: true
        }).bind('slider:changed', function (event, data) {
            jQuery('.eic-lightbox .eic-manipulating-properties #hue-value').html(''+data.value);
        }).bind('slider:release', function (event, data) {
            EasyImageCollage.redrawManipulatedImageProperties();
        });
    jQuery('.eic-lightbox .eic-manipulating-properties #sepia')
        .simpleSlider({
            range: [0,100],
            step: 1,
            snap: true
        }).bind('slider:changed', function (event, data) {
            jQuery('.eic-lightbox .eic-manipulating-properties #sepia-value').html(''+data.value);
        }).bind('slider:release', function (event, data) {
            EasyImageCollage.redrawManipulatedImageProperties();
        });
    jQuery('.eic-lightbox .eic-manipulating-properties #noise')
        .simpleSlider({
            range: [0,100],
            step: 1,
            snap: true
        }).bind('slider:changed', function (event, data) {
            jQuery('.eic-lightbox .eic-manipulating-properties #noise-value').html(''+data.value);
        }).bind('slider:release', function (event, data) {
            EasyImageCollage.redrawManipulatedImageProperties();
        });
    jQuery('.eic-lightbox .eic-manipulating-properties #sharpen')
        .simpleSlider({
            range: [0,100],
            step: 1,
            snap: true
        }).bind('slider:changed', function (event, data) {
            jQuery('.eic-lightbox .eic-manipulating-properties #sharpen-value').html(''+data.value);
        }).bind('slider:release', function (event, data) {
            EasyImageCollage.redrawManipulatedImageProperties();
        });
    jQuery('.eic-lightbox .eic-manipulating-properties #blur')
        .simpleSlider({
            range: [0,20],
            step: 1,
            snap: true
        }).bind('slider:changed', function (event, data) {
            jQuery('.eic-lightbox .eic-manipulating-properties #blur-value').html(''+data.value);
        }).bind('slider:release', function (event, data) {
            EasyImageCollage.redrawManipulatedImageProperties();
        });
};