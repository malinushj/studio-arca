<a href="#" class="eic-modal-title" onclick="event.preventDefault(); EasyImageCollage.setActivePage('editing')"><i class="fa fa-angle-double-left"></i> <?php _e( 'Cancel', 'easy-image-collage' ); ?></a>
<a href="#" class="eic-modal-title eic-modal-title-right" onclick="event.preventDefault(); EasyImageCollage.btnManipulationSave()"><?php _e( 'Save Changes', 'easy-image-collage' ); ?> <i class="fa fa-angle-double-right"></i></a>

<div class="eic-manipulating-filters-placeholder">
    <div class="eic-property-header"><?php _e( 'Filters', 'easy-image-collage' ); ?></div>
    <span class="eic-manipulating-filter" data-filter="vintage">Vintage</span>
    <span class="eic-manipulating-filter" data-filter="sunrise">Sunrise</span>
    <span class="eic-manipulating-filter" data-filter="lomo">Lomo</span>
    <span class="eic-manipulating-filter" data-filter="nostalgia">Nostalgia</span>
    <span class="eic-manipulating-filter" data-filter="clarity">Clarity</span>
    <span class="eic-manipulating-filter" data-filter="pinhole">Pinhole</span>
    <span class="eic-manipulating-filter" data-filter="grungy">Grungy</span>
    <span class="eic-manipulating-filter" data-filter="love">Love</span>
    <span class="eic-manipulating-filter" data-filter="hemingway">Hemingway</span>
    <span class="eic-manipulating-filter" data-filter="concentrate">Concentrate</span>
</div>
<div class="eic-manipulating-properties-placeholder">
	<div class="eic-property-header"><?php _e( 'Properties', 'easy-image-collage' ); ?></div>
	<span class="eic-property-label"><?php _e( 'Brightness', 'easy-image-collage' ); ?></span>
	<input type="text" class="eic-slider" id="brightness" value="0">
	<span class="eic-slider-value"><span id="brightness-value">0</span></span>

	<span class="eic-property-label"><?php _e( 'Contrast', 'easy-image-collage' ); ?></span>
	<input type="text" class="eic-slider" id="contrast" value="0">
	<span class="eic-slider-value"><span id="contrast-value">0</span></span>

    <span class="eic-property-label"><?php _e( 'Saturation', 'easy-image-collage' ); ?></span>
    <input type="text" class="eic-slider" id="saturation" value="0">
    <span class="eic-slider-value"><span id="saturation-value">0</span></span>

    <span class="eic-property-label"><?php _e( 'Exposure', 'easy-image-collage' ); ?></span>
    <input type="text" class="eic-slider" id="exposure" value="0">
    <span class="eic-slider-value"><span id="exposure-value">0</span></span>

    <span class="eic-property-label"><?php _e( 'Vibrance', 'easy-image-collage' ); ?></span>
    <input type="text" class="eic-slider" id="vibrance" value="0">
    <span class="eic-slider-value"><span id="vibrance-value">0</span></span>

    <br/><br/>

    <span class="eic-property-label"><?php _e( 'Hue', 'easy-image-collage' ); ?></span>
    <input type="text" class="eic-slider" id="hue" value="0">
    <span class="eic-slider-value"><span id="hue-value">0</span></span>

    <span class="eic-property-label"><?php _e( 'Sepia', 'easy-image-collage' ); ?></span>
    <input type="text" class="eic-slider" id="sepia" value="0">
    <span class="eic-slider-value"><span id="sepia-value">0</span></span>

    <span class="eic-property-label"><?php _e( 'Noise', 'easy-image-collage' ); ?></span>
    <input type="text" class="eic-slider" id="noise" value="0">
    <span class="eic-slider-value"><span id="noise-value">0</span></span>

    <span class="eic-property-label"><?php _e( 'Sharpen', 'easy-image-collage' ); ?></span>
    <input type="text" class="eic-slider" id="sharpen" value="0">
    <span class="eic-slider-value"><span id="sharpen-value">0</span></span>

    <span class="eic-property-label"><?php _e( 'Blur', 'easy-image-collage' ); ?></span>
    <input type="text" class="eic-slider" id="blur" value="0">
    <span class="eic-slider-value"><span id="blur-value">0</span></span>
</div>

<table>
	<tbody>
	<tr>
        <td>
            <div class="eic-manipulating-filters"></div>
            <div class="eic-manipulating-image">
                <img id="eic-manipulating-image-element"/>
            </div>
        </td>
        <td class="eic-manipulating-properties"></td>
	</tr>
	</tbody>
</table>