<?php

class EIC_Captions extends EIC_Premium_Addon {

    public function __construct( $name = 'captions' ) {
        parent::__construct( $name );

        // Actions
	    add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin' ) );
    }

	public function enqueue_admin()
	{
		$screen = get_current_screen();

		if( $screen->base == 'post' ) {
			// Plugin assets
			wp_enqueue_script( 'eic_captions', $this->addonUrl . '/js/captions.js', array( 'eic_admin' ), EIC_VERSION, true );
		}
	}
}

EasyImageCollage::loaded_addon( 'captions', new EIC_Captions() );