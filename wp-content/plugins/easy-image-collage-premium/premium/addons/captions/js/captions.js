var EasyImageCollage = EasyImageCollage || {};

EasyImageCollage.loadCaptions = function(id) {
    var grid = EasyImageCollage.editing_grid;

    var table = jQuery('.eic-lightbox .eic-captions-images');
    var table_contents = '';

    for(var i = 0; i < grid['images'].length; i++) {
        var image = grid['images'][i];

        if(image) {
            var src = image.attachment_thumb == undefined ? image.attachment_url : image.attachment_thumb;
            var text = image.custom_caption == undefined ? '' : image.custom_caption;

            table_contents += '<tr data-image="' + i + '">';
            table_contents += '<td><img src="' + src + '"></td>';
            table_contents += '<td>';
            table_contents += '<input type="text" id="eic-captions-images-caption-' + i + '" class="eic-captions-images-caption" value="' + text + '">';
            table_contents += '</td>';
            table_contents += '</tr>';
        }
    }

    table.html(table_contents);
    jQuery('#eic-captions-images-caption-' + id).focus();
};

EasyImageCollage.btnCaptionsSave = function() {
    var grid = EasyImageCollage.editing_grid;

    for(var i = 0; i < grid['images'].length; i++) {
        var image = grid['images'][i];

        if(image) {
            image.custom_caption = jQuery('#eic-captions-images-caption-' + i).val();

            EasyImageCollage.editing_grid['images'][i] = image;
        }
    }

    EasyImageCollage.setActivePage('editing');
};