var EasyImageCollage = EasyImageCollage || {};

EasyImageCollage.custom_layout_grid = {};
EasyImageCollage.custom_layout_divider_id = 0;

EasyImageCollage.customLayout = function() {
    var placeholder = jQuery('.eic-lightbox .eic-creating .eic-image-placeholder')[0].outerHTML;
    placeholder = placeholder.replace(/eic_image_id/gm, 0);

    EasyImageCollage.custom_layout_grid = {};
    jQuery('.eic-lightbox .eic-creating .eic-frame').html(placeholder);
    EasyImageCollage.resetIDs();
    EasyImageCollage.recalculateSizes();
};

EasyImageCollage.btnSplitRows = function(id) { EasyImageCollage.btnSplit(id, '.eic-rows' ); };
EasyImageCollage.btnSplitCols = function(id) { EasyImageCollage.btnSplit(id, '.eic-cols' ); };

EasyImageCollage.btnSplit = function(id, placeholder_id) {
    var image_to_split = jQuery('.eic-lightbox .eic-creating .eic-image-' + id);
    var placeholder = jQuery('.eic-lightbox .eic-creating .eic-placeholder ' + placeholder_id)[0].outerHTML;

    placeholder = placeholder.replace(/eic_divider_id/gm, EasyImageCollage.custom_layout_divider_id);
    image_to_split.replaceWith(placeholder);

    EasyImageCollage.handleDividerMove(jQuery('.eic-lightbox .eic-creating .eic-divider-' + EasyImageCollage.custom_layout_divider_id), EasyImageCollage.custom_layout_grid);
    EasyImageCollage.custom_layout_divider_id++;

    EasyImageCollage.resetIDs();
    EasyImageCollage.recalculateSizes();
};

EasyImageCollage.resetIDs = function() {
    jQuery('.eic-lightbox .eic-creating .eic-frame .eic-image').each(function(index, elem) {
        var image = jQuery(this);

        image.removeClass()
            .addClass('eic-image')
            .addClass('eic-image-' + index)
            .data('image-id', index);

        image.find('.eic-image-control-split-rows').attr('onclick', 'EasyImageCollage.btnSplitRows(' + index + ')');
        image.find('.eic-image-control-split-cols').attr('onclick', 'EasyImageCollage.btnSplitCols(' + index + ')');
        image.find('.eic-image-control-image').attr('onclick', 'EasyImageCollage.btnImage(' + index + ')');
        image.find('.eic-image-control-manipulate').attr('onclick', 'EasyImageCollage.btnManipulate(' + index + ')');
        image.find('.eic-image-control-link').attr('onclick', 'EasyImageCollage.btnLink(' + index + ')');
        image.find('.eic-image-control-caption').attr('onclick', 'EasyImageCollage.btnCaption(' + index + ')');
    });
};

EasyImageCollage.btnCustomLayoutSave = function() {
    var frame = jQuery('.eic-lightbox .eic-creating .eic-frame'),
        layout = EasyImageCollage.getLayoutChild(frame);

    EasyImageCollage.btnPickLayout(frame, layout);
};

EasyImageCollage.getLayoutChild = function(elem) {
    var child = elem.children().first();

    if(child.hasClass('eic-image')) {
        return {
            type: 'img',
            id: parseInt(child.data('image-id'))
        }
    } else {
        var type = child.hasClass('eic-rows') ? 'row' : 'col',
            id = parseInt(child.children('.eic-divider').data('divider-id')),
            pos = (EasyImageCollage.custom_layout_grid['dividers'] == undefined || EasyImageCollage.custom_layout_grid['dividers'][id] == undefined) ? 0.5 : EasyImageCollage.custom_layout_grid['dividers'][id];

        return {
            type: type,
            pos: pos,
            id: id,
            children: [
                EasyImageCollage.getLayoutChild(child.children('.eic-child-1')),
                EasyImageCollage.getLayoutChild(child.children('.eic-child-2'))
            ]
        }
    }
};