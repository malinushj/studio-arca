<a href="#" class="eic-modal-title" onclick="event.preventDefault(); EasyImageCollage.btnChooseLayout()"><i class="fa fa-angle-double-left"></i> <?php _e( 'Cancel', 'easy-image-collage' ); ?></a>
<a href="#" class="eic-modal-title eic-modal-title-right" onclick="event.preventDefault(); EasyImageCollage.btnCustomLayoutSave()"><?php _e( 'Save Changes', 'easy-image-collage' ); ?> <i class="fa fa-angle-double-right"></i></a>

<div class="eic-container">
    <div class="eic-frame eic-frame-0 eic-frame-custom" data-layout-name="custom">
        <div class="eic-image eic-image-0">
            <div class="eic-image-size">
                <span class="eic-image-width">0</span> x <span class="eic-image-height">0</span>
            </div>
            <div class="eic-image-controls">
                <div class="eic-image-control eic-image-control-split-rows" onclick="event.preventDefault(); EasyImageCollage.btnSplitRows(0)"><i class="fa fa-columns fa-rotate-270"></i></div>
                <div class="eic-image-control eic-image-control-split-cols" onclick="event.preventDefault(); EasyImageCollage.btnSplitCols(0)"><i class="fa fa-columns"></i></div>
                <div class="eic-image-control eic-image-control-image" onclick="event.preventDefault(); EasyImageCollage.btnImage(0)"><i class="fa fa-picture-o"></i></div>
                <div class="eic-image-control eic-image-control-manipulate" onclick="event.preventDefault(); EasyImageCollage.btnManipulate(0)"><i class="fa fa-wrench"></i></div>
                <div class="eic-image-control eic-image-control-link" onclick="event.preventDefault(); EasyImageCollage.btnLink(0)"><i class="fa fa-link"></i></div>
                <div class="eic-image-control eic-image-control-caption" onclick="event.preventDefault(); EasyImageCollage.btnCaption(0)"><i class="fa fa-font"></i></div>
            </div>
        </div>
    </div>
</div>

<div class="eic-placeholder">
    <div class="eic-image eic-image-placeholder">
        <div class="eic-image-size">
            <span class="eic-image-width">0</span> x <span class="eic-image-height">0</span>
        </div>
        <div class="eic-image-controls">
            <div class="eic-image-control eic-image-control-split-rows" onclick="event.preventDefault(); EasyImageCollage.btnSplitRows()"><i class="fa fa-columns fa-rotate-270"></i></div>
            <div class="eic-image-control eic-image-control-split-cols" onclick="event.preventDefault(); EasyImageCollage.btnSplitCols()"><i class="fa fa-columns"></i></div>
            <div class="eic-image-control eic-image-control-image" onclick="event.preventDefault(); EasyImageCollage.btnImage()"><i class="fa fa-picture-o"></i></div>
            <div class="eic-image-control eic-image-control-manipulate" onclick="event.preventDefault(); EasyImageCollage.btnManipulate()"><i class="fa fa-wrench"></i></div>
            <div class="eic-image-control eic-image-control-link" onclick="event.preventDefault(); EasyImageCollage.btnLink()"><i class="fa fa-link"></i></div>
            <div class="eic-image-control eic-image-control-caption" onclick="event.preventDefault(); EasyImageCollage.btnCaption()"><i class="fa fa-font"></i></div>
        </div>
    </div>
    <div class="eic-rows">
        <div class="eic-row eic-child-1" style="top: 0; left: 0; right: 0; bottom: 50%; height: 50%;">
            <div class="eic-image">
                <div class="eic-image-size">
                    <span class="eic-image-width">0</span> x <span class="eic-image-height">0</span>
                </div>
                <div class="eic-image-controls">
                    <div class="eic-image-control eic-image-control-split-rows" onclick="event.preventDefault(); EasyImageCollage.btnSplitRows()"><i class="fa fa-columns fa-rotate-270"></i></div>
                    <div class="eic-image-control eic-image-control-split-cols" onclick="event.preventDefault(); EasyImageCollage.btnSplitCols()"><i class="fa fa-columns"></i></div>
                    <div class="eic-image-control eic-image-control-image" onclick="event.preventDefault(); EasyImageCollage.btnImage()"><i class="fa fa-picture-o"></i></div>
                    <div class="eic-image-control eic-image-control-manipulate" onclick="event.preventDefault(); EasyImageCollage.btnManipulate()"><i class="fa fa-wrench"></i></div>
                    <div class="eic-image-control eic-image-control-link" onclick="event.preventDefault(); EasyImageCollage.btnLink()"><i class="fa fa-link"></i></div>
                    <div class="eic-image-control eic-image-control-caption" onclick="event.preventDefault(); EasyImageCollage.btnCaption()"><i class="fa fa-font"></i></div>
                </div>
            </div>
        </div>
        <div class="eic-divider eic-divider-row eic-divider-eic_divider_id" style="left: 10%; right: 0; top: 50%; height: 4px; width: 80%; margin-top: -2px; cursor: row-resize;" data-divider-type="row" data-divider-id="eic_divider_id"></div>
        <div class="eic-row eic-child-2" style="bottom: 0; left: 0; right: 0; top: 50%; height: 50%;">
            <div class="eic-image">
                <div class="eic-image-size">
                    <span class="eic-image-width">0</span> x <span class="eic-image-height">0</span>
                </div>
                <div class="eic-image-controls">
                    <div class="eic-image-control eic-image-control-split-rows" onclick="event.preventDefault(); EasyImageCollage.btnSplitRows()"><i class="fa fa-columns fa-rotate-270"></i></div>
                    <div class="eic-image-control eic-image-control-split-cols" onclick="event.preventDefault(); EasyImageCollage.btnSplitCols()"><i class="fa fa-columns"></i></div>
                    <div class="eic-image-control eic-image-control-image" onclick="event.preventDefault(); EasyImageCollage.btnImage()"><i class="fa fa-picture-o"></i></div>
                    <div class="eic-image-control eic-image-control-manipulate" onclick="event.preventDefault(); EasyImageCollage.btnManipulate()"><i class="fa fa-wrench"></i></div>
                    <div class="eic-image-control eic-image-control-link" onclick="event.preventDefault(); EasyImageCollage.btnLink()"><i class="fa fa-link"></i></div>
                    <div class="eic-image-control eic-image-control-caption" onclick="event.preventDefault(); EasyImageCollage.btnCaption()"><i class="fa fa-font"></i></div>
                </div>
            </div>
        </div>
    </div>
    <div class="eic-cols">
        <div class="eic-col eic-child-1" style="top: 0; bottom: 0; left: 0; right: 50%; width: 50%;">
            <div class="eic-image">
                <div class="eic-image-size">
                    <span class="eic-image-width">0</span> x <span class="eic-image-height">0</span>
                </div>
                <div class="eic-image-controls">
                    <div class="eic-image-control eic-image-control-split-rows" onclick="event.preventDefault(); EasyImageCollage.btnSplitRows()"><i class="fa fa-columns fa-rotate-270"></i></div>
                    <div class="eic-image-control eic-image-control-split-cols" onclick="event.preventDefault(); EasyImageCollage.btnSplitCols()"><i class="fa fa-columns"></i></div>
                    <div class="eic-image-control eic-image-control-image" onclick="event.preventDefault(); EasyImageCollage.btnImage()"><i class="fa fa-picture-o"></i></div>
                    <div class="eic-image-control eic-image-control-manipulate" onclick="event.preventDefault(); EasyImageCollage.btnManipulate()"><i class="fa fa-wrench"></i></div>
                    <div class="eic-image-control eic-image-control-link" onclick="event.preventDefault(); EasyImageCollage.btnLink()"><i class="fa fa-link"></i></div>
                    <div class="eic-image-control eic-image-control-caption" onclick="event.preventDefault(); EasyImageCollage.btnCaption()"><i class="fa fa-font"></i></div>
                </div>
            </div>
        </div>
        <div class="eic-divider eic-divider-col eic-divider-eic_divider_id" style="top: 10%; bottom: 0; left: 50%; height: 80%; width: 4px; margin-left: -2px; cursor: col-resize;" data-divider-type="col" data-divider-id="eic_divider_id"></div>
        <div class="eic-col eic-child-2" style="top: 0; bottom: 0; right: 0; left: 50%; width: 50%;">
            <div class="eic-image">
                <div class="eic-image-size">
                    <span class="eic-image-width">0</span> x <span class="eic-image-height">0</span>
                </div>
                <div class="eic-image-controls">
                    <div class="eic-image-control eic-image-control-split-rows" onclick="event.preventDefault(); EasyImageCollage.btnSplitRows()"><i class="fa fa-columns fa-rotate-270"></i></div>
                    <div class="eic-image-control eic-image-control-split-cols" onclick="event.preventDefault(); EasyImageCollage.btnSplitCols()"><i class="fa fa-columns"></i></div>
                    <div class="eic-image-control eic-image-control-image" onclick="event.preventDefault(); EasyImageCollage.btnImage()"><i class="fa fa-picture-o"></i></div>
                    <div class="eic-image-control eic-image-control-manipulate" onclick="event.preventDefault(); EasyImageCollage.btnManipulate()"><i class="fa fa-wrench"></i></div>
                    <div class="eic-image-control eic-image-control-link" onclick="event.preventDefault(); EasyImageCollage.btnLink()"><i class="fa fa-link"></i></div>
                    <div class="eic-image-control eic-image-control-caption" onclick="event.preventDefault(); EasyImageCollage.btnCaption()"><i class="fa fa-font"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>