<?php

class EIC_Custom_Layout extends EIC_Premium_Addon {

    public function __construct( $name = 'custom-layout' ) {
        parent::__construct( $name );

        // Actions
	    add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin' ) );

        // AJAX
        add_action( 'wp_ajax_custom_layout', array( $this, 'ajax_custom_layout' ) );
    }

	public function enqueue_admin()
	{
		$screen = get_current_screen();

		if( $screen->base == 'post' ) {
			// Plugin assets
			wp_enqueue_script( 'eic_custom_layout', $this->addonUrl . '/js/custom-layout.js', array( 'eic_admin' ), EIC_VERSION, true );
		}
	}

    public function ajax_custom_layout()
    {
        if( check_ajax_referer( 'eic_image_collage', 'security', false ) )
        {
            $layout = $_POST['layout'];

            var_dump( $layout );
        }

        die();
    }
}

EasyImageCollage::loaded_addon( 'custom-layout', new EIC_Custom_Layout() );