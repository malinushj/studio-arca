// Defines which version to build
var dev = process.env.NODE_ENV !== 'production';

// Plugins used by webpack
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    entry: [
        __dirname + "/src/ts/index.ts"
    ],
    output: {
        path: __dirname + "/build",
        filename: "build.js"
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'awesome-typescript-loader',
                exclude: /node_modules/
            },
            {
                test: /\.sass$/,
                loaders: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{
                        loader: "css-loader", options: {
                            sourceMap: true,
                            url: false
                        }
                    }, {
                        loader: "sass-loader", options: {
                            sourceMap: true
                        }
                    }]
                })
            }
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ]
    },
    plugins: [
        // General plugins (dev + production)
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            EIC_Responsive: 'EIC_Responsive'
        }),
        new ExtractTextPlugin('../build/build.css')
    ].concat(
        dev ? [
            // Plugins used for development
            new BrowserSyncPlugin(
                {
                    files: [
                        "**/*.php",
                        "**/*.twig"
                    ],
                    host: 'localhost',
                    port: 3000,
                    proxy: 'http://studio-arca.test/'
                },
                {
                    reload: true,
                    injectCss: true
                })
        ] : [
            // Plugins used for production
            new webpack.optimize.ModuleConcatenationPlugin(),
            new webpack.optimize.UglifyJsPlugin({
                compress: { warnings: false },
                include: /\.js$/
            }),
            new OptimizeCssAssetsPlugin({
                assetNameRegExp: /\.css$/,
                cssProcessorOptions: { discardComments: { removeAll: true } }
            })
        ]
    ),
    externals: {
        jquery: 'jQuery',
        EIC_Responsive: 'EIC_Responsive'
    },
    devtool: dev ? 'source-map' : false, // Create sourcemaps for css and js in development
    watch: dev, // Use webpacks watcher in development
    watchOptions: {
        aggregateTimeout: 500,
        poll: true
    }
}
