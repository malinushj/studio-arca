<?php

namespace StudioArca;
use Timber\Timber;

$context = Timber::get_context();
$context['project'] = Timber::get_post(false, Project::class);

Timber::render('single-projects.twig', $context);
