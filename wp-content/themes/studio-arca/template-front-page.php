<?php
/**
 * Template Name: Front Page
 */

namespace StudioArca;

use Timber\Timber;

$context = Timber::get_context();
$context['post'] = Timber::get_post();

if ($latest = get_field('latest_updates')) {
    $latest = array_map(function($id) {
        return Timber::get_post($id);
    }, $latest);

    $context['latest'] = $latest;
}

Timber::render('front-page.twig', $context);
