<?php

namespace StudioArca;
/**
 * Initialize Composer
 */
require_once(__DIR__ . '/vendor/autoload.php');

/**
 * Initialize Theme
 */
ThemeFactory::init();

/**
 * Instantiate Global Timber Variable
 * @see index.php
 */
$timber = new \Timber\Timber();
