<?php

namespace StudioArca;
use Timber\Timber;

$context = Timber::get_context();
$context['posts'] = Timber::get_posts(['numberposts' => -1]);
$context['years'] = Helper::get_posts_years();

Timber::render('archive-posts.twig', $context);
