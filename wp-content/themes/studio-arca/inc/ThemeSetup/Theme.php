<?php

namespace StudioArca\ThemeSetup;

class Theme implements ThemeSetupInterface
{
    const STRING_NAMESPACE = "Studio Arca";
    public static function register()
    {
        add_action('after_setup_theme', [__CLASS__, 'theme_support']);
        add_action('after_setup_theme', [__CLASS__, 'menus']);
        add_action('wp_enqueue_scripts', [__CLASS__, 'assets']);
        add_action('admin_menu', [__CLASS__, 'remove_comments_admin_bar']);
        add_action('acf/init', [__CLASS__, 'acf_google_map_api']);

        add_filter('the_content', [__CLASS__, 'filter_ptags_on_images']);
        add_filter('shortcode_atts_gallery', [__CLASS__, 'remove_gallery_link']);
        add_filter('use_default_gallery_style', '__return_false');

        add_shortcode('contact', [__CLASS__, 'render_contact_shortcode']);

        self::register_string_translations();

        if( function_exists('acf_add_options_page') ) {
            acf_add_options_page(array(
                'page_title' => 'Theme General Settings',
                'menu_title' => 'Theme Settings',
                'menu_slug' => 'theme-general-settings',
                'capability' => 'edit_posts',
                'redirect' => false
            ));
        }
    }

    private static function register_string_translations()
    {
        if (!function_exists('pll_register_string')) return;

        pll_register_string('All Filters', 'All', self::STRING_NAMESPACE);
    }

    public static function theme_support()
    {
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );
    }

    public static function menus()
    {
        register_nav_menus(
             [ 'header-menu' => __( 'Header Menu' ) ]
        );
    }

    // Remove Comments section from admin menu
    public static function remove_comments_admin_bar()
    {
        remove_menu_page( 'edit-comments.php' );
    }


    public static function filter_ptags_on_images($content)
    {
        return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
    }

    // Google Maps API Key for ACF
    public static function acf_google_map_api()
    {
        if(defined('GMAPS_API'))
            acf_update_setting('google_api_key', GMAPS_API);
    }

    public static function assets()
    {
        wp_enqueue_style('dev', get_stylesheet_directory_uri() . '/build/build.css');
        wp_enqueue_script('dev', get_template_directory_uri() . '/build/build.js', array('jquery'), '', true);

        if(defined('GMAPS_API'))
            wp_enqueue_script('map', '//maps.googleapis.com/maps/api/js?key=' . GMAPS_API);
    }

    public static function remove_gallery_link($atts) {
        $atts['link'] = 'none';
        return $atts;
    }

    public static function render_contact_shortcode() {
        $context = \Timber\Timber::get_context();
        return TimberContext::render_contact_view($context);
    }
}
