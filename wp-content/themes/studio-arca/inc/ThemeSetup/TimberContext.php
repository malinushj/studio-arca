<?php

namespace StudioArca\ThemeSetup;

class TimberContext implements ThemeSetupInterface
{
    public static function register()
    {
        add_filter('timber/context', [__CLASS__, 'add_menu_to_context']);
        add_filter('timber/context', [__CLASS__, 'add_footer_links_to_context']);
        add_filter('timber/context', [__CLASS__, 'add_map_to_context']);
        add_filter('timber/context', [__CLASS__, 'add_should_display_filters']);
    }

    public static function render_contact_view($context): string
    {
        $links = array_reverse($context['social_links']);
        $context['contact_infos'] = array_filter($links, function($link) {
            return $link['type'] === "Address" || $link['type'] === "Email" || $link['type'] === "Telephone";
        });

        return \Timber\Timber::compile('partials/contact.twig', $context);
    }

    private static function get_lang_switcher()
    {
        ob_start();
        pll_the_languages([
            'display_names_as' => 'slug',
            'show_names' => false,
            'hide_current' => 1
        ]);
        return ob_get_clean();
    }

    public static function add_menu_to_context($context)
    {
        $context['menu'] = new \Timber\Menu( 'header-menu' );
        $context['lang'] = self::get_lang_switcher();
        return $context;
    }

    private static function format_link(string $link, string $type): string
    {
        switch($type)
        {
            case "Social":
                $link = esc_url($link);
                break;
            case "Address":
                $link = esc_url(sprintf(
                    "https://www.google.com/maps/search/?api=1&query=%s",
                    preg_replace("/(?![=$'€%-])\p{P}/u", "", $link) // strip punctuation
                ));
                break;
            case "Email":
                $link = sprintf("mailto:%s", $link);
                break;
            case "Telephone":
                $link = sprintf("tel:%s", preg_replace("/(?![.=$'€%])\p{P}/u", "", $link));
                break;
            default: break;
        }
        return preg_replace('/\s+/', '', $link); // strip whitespaces
    }

    public static function add_footer_links_to_context($context)
    {
        $context['social_links'] = array_map(function($link) {
            $timberImg = new \Timber\Image($link['icon']);
            $link['icon'] = $timberImg->src();
            $link['label'] = $link['link'];
            $link['link'] = self::format_link($link['link'], $link['type']);
            return $link;
        }, get_field('social_links', 'options'));

        return $context;
    }

    public static function add_map_to_context($context)
    {
        $context['map'] = get_field('map', 'options');
        return $context;
    }

    public static function add_should_display_filters($context)
    {
        $context['show_filters'] = empty(get_field('display_filters', 'options'));
        return $context;
    }
}
