<?php

namespace StudioArca\ThemeSetup;

use StudioArca\Project;

class ProjectPostType implements ThemeSetupInterface
{
    public static function register()
    {
        add_action( 'init', [__CLASS__, 'cpt_projects'], 0 );
        add_action('init', [__CLASS__, 'projects_taxonomy'], 0);
        register_taxonomy_for_object_type('project_taxonomy', 'projects');

        add_filter('Timber\PostClassMap', [__CLASS__, 'map_timber_class']);
    }

    public static function map_timber_class() {
        return ['projects' => Project::class];
    }

    public static function cpt_projects()
    {
        $labels = [
            'name'                  => _x( 'Projects', 'Post Type General Name', 'cpt_projects' ),
            'singular_name'         => _x( 'Project', 'Post Type Singular Name', 'cpt_projects' ),
            'menu_name'             => __( 'Projects', 'cpt_projects' ),
            'name_admin_bar'        => __( 'Project', 'cpt_projects' ),
            'archives'              => __( 'Project Archives', 'cpt_projects' ),
            'attributes'            => __( 'Project Attributes', 'cpt_projects' ),
            'parent_item_colon'     => __( 'Parent Project:', 'cpt_projects' ),
            'all_items'             => __( 'All Projects', 'cpt_projects' ),
            'add_new_item'          => __( 'Add New Project', 'cpt_projects' ),
            'add_new'               => __( 'Add New', 'cpt_projects' ),
            'new_item'              => __( 'New Project', 'cpt_projects' ),
            'edit_item'             => __( 'Edit Project', 'cpt_projects' ),
            'update_item'           => __( 'Update Project', 'cpt_projects' ),
            'view_item'             => __( 'View Project', 'cpt_projects' ),
            'view_items'            => __( 'View Projects', 'cpt_projects' ),
            'search_items'          => __( 'Search Project', 'cpt_projects' ),
            'not_found'             => __( 'Project not found', 'cpt_projects' ),
            'not_found_in_trash'    => __( 'Project not found in Trash', 'cpt_projects' ),
            'featured_image'        => __( 'Featured Image', 'cpt_projects' ),
            'set_featured_image'    => __( 'Set featured image', 'cpt_projects' ),
            'remove_featured_image' => __( 'Remove featured image', 'cpt_projects' ),
            'use_featured_image'    => __( 'Use as featured image', 'cpt_projects' ),
            'insert_into_item'      => __( 'Insert into Project', 'cpt_projects' ),
            'uploaded_to_this_item' => __( 'Uploaded to this Project', 'cpt_projects' ),
            'items_list'            => __( 'Projects list', 'cpt_projects' ),
            'items_list_navigation' => __( 'Projects list navigation', 'cpt_projects' ),
            'filter_items_list'     => __( 'Filter Projects list', 'cpt_projects' ),
        ];

        $rewrite = [
            'slug'                  => 'projects',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
        ];
        $args = [
            'label'                 => __( 'Project', 'cpt_projects' ),
            'description'           => __( 'Studio Arca Projects', 'cpt_projects' ),
            'labels'                => $labels,
            'supports'              => ['title', 'editor', 'thumbnail'],
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-portfolio',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
        ];
        register_post_type('projects', $args);
    }

    public static function projects_taxonomy()
    {
        $labels = [
            'name'                       => _x( 'Project Categories', 'Taxonomy General Name', 'cpt_projects' ),
            'singular_name'              => _x( 'Project Category', 'Taxonomy Singular Name', 'cpt_projects' ),
            'menu_name'                  => __( 'Project Categories', 'cpt_projects' ),
            'all_items'                  => __( 'All Project Categories', 'cpt_projects' ),
            'parent_item'                => __( 'Parent Project Category', 'cpt_projects' ),
            'parent_item_colon'          => __( 'Parent Project Category:', 'cpt_projects' ),
            'new_item_name'              => __( 'New Project Category Name', 'cpt_projects' ),
            'add_new_item'               => __( 'Add New Project Category', 'cpt_projects' ),
            'edit_item'                  => __( 'Edit Project Category', 'cpt_projects' ),
            'update_item'                => __( 'Update Project Category', 'cpt_projects' ),
            'view_item'                  => __( 'View Project Category', 'cpt_projects' ),
            'separate_items_with_commas' => __( 'Separate Project Categories with commas', 'cpt_projects' ),
            'add_or_remove_items'        => __( 'Add or remove Project Categories', 'cpt_projects' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'cpt_projects' ),
            'popular_items'              => __( 'Popular Project Categories', 'cpt_projects' ),
            'search_items'               => __( 'Search Project Categories', 'cpt_projects' ),
            'not_found'                  => __( 'Not Found', 'cpt_projects' ),
            'no_terms'                   => __( 'No Project Categories', 'cpt_projects' ),
            'items_list'                 => __( 'Project Categories list', 'cpt_projects' ),
            'items_list_navigation'      => __( 'Project Categories list navigation', 'cpt_projects' ),
        ];
        $args = [
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
        ];
        register_taxonomy('project_taxonomy', ['projects'], $args);
    }
}
