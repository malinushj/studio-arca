<?php

namespace StudioArca;
use Timber\Post;

class Project extends Post
{
    /**
     * @var string $PostClass the name of the class to handle posts by default
     */
    public $PostClass = __CLASS__;

    protected $_details;

    public function details()
    {
        if (empty($this->_details))
        {
            $this->_details['location'] = [
                'label' => pll__('Location'),
                'value' => get_field('location', $this->ID)
            ];
            $this->_details['type'] = [
                'label' => pll__('Type'),
                'value' => get_field('type', $this->ID)
            ];
            $this->_details['year'] = [
                'label' => pll__('Year'),
                'value' => get_field('year', $this->ID)
            ];
            $this->_details['surface'] = [
                'label' => pll__('Surface'),
                'value' => get_field('surface', $this->ID)
            ];
            $this->_details['stage'] = [
                'label' => pll__('Stage'),
                'value' => get_field('stage', $this->ID)
            ];
        }
        return $this->_details;
    }

    public function terms_slugs()
    {
        $terms = $this->terms('project_taxonomy');
        return array_map(function($term) {
            return $term->slug;
        }, $terms);
    }
}
