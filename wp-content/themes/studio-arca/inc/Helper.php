<?php

namespace StudioArca;

use StudioArca\ThemeSetup\ThemeSetupInterface;

class Helper implements ThemeSetupInterface
{
    public static function register()
    {
        self::add_to_timber();
    }

    public static function get_posts_years() : array
    {
        global $wpdb;
        $YEAR_KEY = "year";

        $query_format = 'SELECT DISTINCT YEAR(%1$s.post_date) AS \'%2$s\' FROM %1$s ORDER BY %1$s.post_date DESC;';
        $query_string = sprintf($query_format, $wpdb->posts, $YEAR_KEY);

        $results = $wpdb->get_results($query_string, ARRAY_A);

        $years = array_map(function($result) use($YEAR_KEY) {
            return $result[$YEAR_KEY];
        }, $results);

        return $years;
    }

    public static function svg_static(string $file) : string
    {
        $url = get_stylesheet_directory() . '/src/img/' . $file . '.svg';
        return self::svg($url);
    }

    public static function svg(string $uri) : string
    {
        return file_get_contents($uri);
    }

    public static function static_image(string $file) : string
    {
        return get_stylesheet_directory_uri() . '/src/img/' . $file;
    }

    private static function add_to_timber()
    {
        add_filter('timber/twig', function( \Twig_Environment $twig )
        {
            $twig->addFunction(new \Twig_SimpleFunction('_', '\pll__'));
            return $twig;
        });

        add_filter('timber/twig', function( \Twig_Environment $twig )
        {
            $twig->addFunction(new \Twig_SimpleFunction('static_image', [__CLASS__, 'static_image']));
            return $twig;
        });

        add_filter('timber/twig', function( \Twig_Environment $twig )
        {
            $twig->addFunction(new \Twig_SimpleFunction('svg_static', [__CLASS__, 'svg_static']));
            return $twig;
        });

        add_filter('timber/twig', function( \Twig_Environment $twig )
        {
            $twig->addFunction(new \Twig_SimpleFunction('svg', [__CLASS__, 'svg']));
            return $twig;
        });
    }
}
