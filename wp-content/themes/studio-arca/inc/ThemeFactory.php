<?php

namespace StudioArca;

class ThemeFactory
{
    private const SETUPS = [
        ThemeSetup\Theme::class,
        ThemeSetup\Dequeue::class,
        ThemeSetup\ProjectPostType::class,
        ThemeSetup\TimberContext::class,
        Helper::class
    ];

    public static function init()
    {
        foreach(self::SETUPS as $setup)
        {
            self::register_setup(new $setup);
        }
    }

    private static function register_setup(ThemeSetup\ThemeSetupInterface $setup)
    {
        $setup::register();
    }
}
