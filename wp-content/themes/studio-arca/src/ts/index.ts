// tslint:disable-next-line:no-reference
/// <reference path="@types/fancybox.d.ts" />
import "jquery-match-height";
import "jquery.easing";
import "../sass/index.sass";

import "@fancyapps/fancybox";
import objectFitImages from "object-fit-images";

import checkPage from "./check-page";

import archiveFilter from "./components/archive-filter";
import eicBorderFix from "./components/eic-border-fix";
import footer from "./components/footer";
import { center_map, new_map } from "./components/gmap-helper";
import heroCarousel from "./components/hero-carousel";
import stickyHeader from "./components/sticky-header";

$(window).on("load", () => {
    $("body").removeClass("loading");
});

$(document).ready(() => {
    objectFitImages("img.objectfit");
    objectFitImages(".content img");

    eicBorderFix();

    $("[rel=lightbox]").each((index: number, item: Element) => {
        $(item).attr("data-fancybox", "gallery");
    });
    $("[data-fancybox=gallery]").fancybox({
        loop: true,
        toolbar: false
    });

    stickyHeader({
        header: $(".header"),
        stickyOffset: $(".header").height()
    });

    $(".js-ham").click(function() {
        $(this).toggleClass("open");
        $(".header").toggleClass("open");
        $(".js-nav").toggleClass("open");
        $(".js-lang").toggleClass("open");
    });

    checkPage("template-front-page", true, () => {
        heroCarousel({
            carouselSelector: ".js-carousel",
            header: $(".header")
        });

        archiveFilter($(".js-grid")[0], ".js-grid-item");
    });

    checkPage("default", true, () => {
        archiveFilter($(".gallery")[0], ".gallery-item");
        const contactMap = $(".js-contact-map");
        if (contactMap.length) {
            const map = new_map(contactMap);
            map.setZoom(15);
            center_map(map);
        }
    });

    checkPage("single-projects", false, () => {
        const detailsClass = ".js-project-details";
        $(detailsClass)
            .next()
            // add the details class (trim the leading .) to let matchHeight do all the calculation
            .addClass(detailsClass.substring(1));
        $(detailsClass).matchHeight();
    });

    checkPage(["post-type-archive-projects", "blog"], false, () => {
        archiveFilter(
            $(".js-grid")[0],
            ".js-grid-item",
            $(".js-filter-buttons"),
            ".js-filter-button"
        );
    });

    footer();
});
