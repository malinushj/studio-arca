export default function(
    pageName: string | string[],
    isTemplate: boolean,
    callback: () => void
): void {
    const shouldExecute =
        typeof pageName === "string"
            ? $("body").hasClass(
                  isTemplate ? `page-template-${pageName}` : pageName
              )
            : pageName.reduce(
                  (acc, page) => acc || $("body").hasClass(page),
                  false
              );

    if (shouldExecute) {
        callback();
    }
}
