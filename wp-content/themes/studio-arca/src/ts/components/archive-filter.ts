import * as Isotope from "isotope-layout";

export default function(
    grid: HTMLElement | null,
    itemSelector: string,
    buttonFilters?: JQuery,
    buttonFilterSelector?: string
) {
    if (!grid) return;

    const isotope = new Isotope(grid, {
        hiddenStyle: {
            opacity: 0,
            transform: "translate3d(0, 10%, 0)"
        },
        itemSelector,
        stagger: 20,
        transitionDuration: 400,
        visibleStyle: {
            opacity: 1,
            transform: "translate3d(0, 0, 0)"
        }
    });

    if (buttonFilters && buttonFilterSelector) {
        onHashchange(isotope, buttonFilters);
        $(window).on("hashchange", () => onHashchange(isotope, buttonFilters));
        buttonFilters.on("click", buttonFilterSelector, function() {
            const filterValue = $(this).attr("data-filter");
            location.hash = encodeURIComponent(filterValue);
        });
    }
}

function onHashchange(isotope: Isotope, buttonFilters: JQuery) {
    const hashFilter = getHashFilter();
    if (!hashFilter) return;
    isotope.arrange({ filter: hashFilter });
    buttonFilters.find(".is-checked").removeClass("is-checked");
    buttonFilters
        .find('[data-filter="' + hashFilter + '"]')
        .addClass("is-checked");
}

function getHashFilter() {
    const hash = location.hash.replace("#", "");
    return hash && decodeURIComponent(hash);
}
