import scrollHelper from "./scroll-helper";

interface IOptions {
    header: JQuery;
    stickyOffset?: number;
    classes?: {
        sticky: string;
        transition: string;
        visible: string;
    };
}

const defaults: IOptions = {
    classes: {
        sticky: "sticky",
        transition: "transition",
        visible: "visible"
    },
    header: $(),
    stickyOffset: 300
};

// Entry point
export default function(options: IOptions) {
    const opts = $.extend({}, defaults, options);

    const headerHeight = opts.header.height();

    const scrollDown = (offsetTop: number) => {
        if (offsetTop >= opts.stickyOffset) {
            opts.header
                .addClass(opts.classes.transition)
                .removeClass(opts.classes.visible);
            setTimeout(() => opts.header.addClass(opts.classes.sticky), 200);
        }
    };

    const scrollUp = (offsetTop: number) => {
        if (offsetTop > 0) {
            opts.header.addClass(opts.classes.visible);
        } else {
            opts.header.removeClass(opts.classes.transition);

            setTimeout(() => {
                opts.header.removeClass(opts.classes.sticky);
            }, 200);
        }
    };

    scrollHelper(scrollUp, scrollDown);
}
