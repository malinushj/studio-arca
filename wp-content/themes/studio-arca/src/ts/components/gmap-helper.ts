// ACF Google Maps
import mapStyles from "../map-styles.json";

const markers: google.maps.Marker[] = [];

export function new_map($el: JQuery): google.maps.Map {
    const $markers = $el.find(".marker");
    const args = {
        center: new google.maps.LatLng(0, 0),
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: mapStyles,
        zoom: 15
    };
    const map = new google.maps.Map($el[0], args);
    $markers.each(function() {
        add_marker($(this), map);
    });

    return map;
}

function add_marker($marker: JQuery, map: google.maps.Map): void {
    const latlng = new google.maps.LatLng(
        $marker.data("lat"),
        $marker.data("lng")
    );
    // create marker
    const marker = new google.maps.Marker({
        icon: $marker.data("icon"),
        map,
        position: latlng
    });
    // add to array
    markers.push(marker);
    // if marker contains HTML, add it to an infoWindow
    if ($marker.html()) {
        // create info window
        const infowindow = new google.maps.InfoWindow({
            content: $marker.html()
        });
        // show info window when marker is clicked
        google.maps.event.addListener(marker, "click", () => {
            infowindow.open(map, marker);
        });
    }
}

export function center_map(map: google.maps.Map): void {
    const bounds = new google.maps.LatLngBounds();
    // loop through all markers and create bounds
    markers.map(marker =>
        bounds.extend({
            lat: marker.getPosition().lat(),
            lng: marker.getPosition().lng()
        })
    );
    // only 1 marker?
    if (markers.length === 1) {
        // set center of map
        map.setCenter(bounds.getCenter());
    } else {
        // fit to bounds
        map.fitBounds(bounds);
    }
}
