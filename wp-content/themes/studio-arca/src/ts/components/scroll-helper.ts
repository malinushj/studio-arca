let lastScroll = -1;
const rAF = window.requestAnimationFrame || window.webkitRequestAnimationFrame;

let callbackUp: (YOffset: number) => void;
let callbackDown: (YOffset: number) => void;

export default function scroll(
    cbUp: (YOffset: number) => void,
    cbDown: (YOffset: number) => void
) {
    callbackUp = cbUp;
    callbackDown = cbDown;
    scrollHandler();
}

function scrollHandler() {
    // Prevent extra calculations
    if (lastScroll !== window.pageYOffset) {
        // Going up/down
        if (window.pageYOffset > lastScroll) {
            callbackDown(window.pageYOffset);
        } else {
            callbackUp(window.pageYOffset);
        }

        lastScroll = window.pageYOffset;
    } else {
        rAF(scrollHandler);
        return false;
    }

    rAF(scrollHandler);
}
