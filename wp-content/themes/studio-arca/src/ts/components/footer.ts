import { center_map, new_map } from "./gmap-helper";
import { isMobile } from "./helpers";

export default function() {
    gmap();
    contact();

    $(".overlay").click(closeModals);
    $(".close").click(closeModals);

    $(".js-arrow-up").click(() =>
        $("html, body")
            .stop()
            .animate({ scrollTop: 0 }, 200, "easeOutExpo")
    );
}

function contact() {
    footerModal($(".js-Email, .js-Telephone"), context => {
        if ($(".js-footer-detail.open", context).length === 0) {
            $(".overlay").fadeIn(250);
            $(context).addClass("parent-open");
            $(context)
                .find(".js-footer-detail")
                .addClass("open")
                .fadeIn(200);
        }
    });
}

function gmap() {
    const map = new_map($(".js-map"));

    footerModal($(".js-Address"), context => {
        if ($(".js-footer-detail.open", context).length === 0) {
            $(".overlay").fadeIn(250);
            $(context)
                .find(".js-footer-detail")
                .addClass("open")
                .fadeIn(200);
            google.maps.event.trigger(map, "resize");
            center_map(map);
            map.setZoom(15);
        }
    });
}

function footerModal($el: JQuery, callback: (context: HTMLElement) => void) {
    $el.click(function(e) {
        if (!isMobile()) {
            e.preventDefault();
            callback(this);
        }
    });
}

function closeModals(): void {
    $(".overlay").fadeOut(200);
    $(".js-footer-detail.open").fadeOut(200, function() {
        $(this).removeClass("open");
    });
    $("a.parent-open").removeClass("parent-open");
}
