import * as Flickity from "flickity";

interface ICarouselConfig {
    header: JQuery;
    carouselSelector: string;
}

export default function(config: ICarouselConfig) {
    const carouselEl = document.querySelector(config.carouselSelector);

    if (!carouselEl) return;

    const carousel = new Flickity(carouselEl, {
        arrowShape: {
            x0: 20,
            x1: 60,
            x2: 65,
            x3: 30,
            y1: 50,
            y2: 45
        },
        autoPlay: 2000,
        wrapAround: true
    });
}
