// tslint:disable-next-line:class-name
declare class EIC_Responsive {
    static checkBreakpointOfAllElements(): void;
}

export default function() {
    if (!EIC_Responsive) return;

    $(".eic-container").each((index: number, item: Element) => {
        const container = $(item);
        const frame = container.find(".eic-frame");
        const border = frame.data("orig-border");

        container.css({
            "margin-left": `-${border}px`,
            "max-width": "initial",
            "width": `calc(100% + ${border}px)`,
        });

        EIC_Responsive.checkBreakpointOfAllElements();
    });
}
