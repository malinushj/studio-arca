export const isMobile = (): boolean => matchMedia("(max-width: 800px)").matches;
