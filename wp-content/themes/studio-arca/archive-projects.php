<?php

namespace StudioArca;
use Timber\Timber;

$context = Timber::get_context();
$context['projects'] = Timber::get_posts(false, Project::class);
$context['terms'] = get_terms([
        'taxonomy' => 'project_taxonomy',
        'hide_empty' => true
    ]);

Timber::render('archive-projects.twig', $context);
