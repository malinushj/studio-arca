<?php

namespace StudioArca;

/**
 * This theme uses Timber to render templates,
 * for documentation and details about how this theme works
 *
 * @see https://github.com/timber/timber
 *
 */

$context = \Timber::get_context();
$context['post'] = \Timber::get_post();

\Timber::render('index.twig', $context);
